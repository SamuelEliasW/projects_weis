﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Security.Cryptography;
using System.Windows;
using System.Xml.Serialization;

namespace Lernprogram
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		static string FileName = @"..\..\Questions.xml";
		public static List<Subject> subjectMap = new List<Subject>();
		private string choosensubject;
		private string choosentheme;
		private int index = 0;
		private int length;
		private int rightanswer = 0;
		bool onlyonce = true;
		bool activateThemeBox = true;
		public MainWindow()
		{
			InitializeComponent();

			subjectMap = (List<Subject>)DeSerializeItem(subjectMap.GetType());
			subjectBox.ItemsSource = GetSubjects();
		}

		private void NewQuestion_Click(object sender, RoutedEventArgs e)
		{
			 UserControl1 newWindow = new UserControl1();
			newWindow.Show();
		}

		private static object DeSerializeItem(Type t)
		{
			XmlSerializer serializer = new XmlSerializer(t);
			FileStream fs = new FileStream(FileName, FileMode.Open);

			return serializer.Deserialize(fs);
		}

		public string[] GetSubjects()
		{
			string[] subjectnames = { "no recipes" };
			if (subjectMap != null)
			{
				subjectnames = new string[subjectMap.Count];
				for (int i = 0; i < subjectMap.Count; i++)
				{
					subjectnames[i] = subjectMap[i]._subjectname;
				}

			}
			return subjectnames;
		}

		public string[] GetThemes()
		{
			string[] themenames = { "no themes" };

			themenames = new string[subjectMap.Count];
			for (int i = 0; i < subjectMap.Count; i++)
			{
				if (subjectMap[i]._subjectname == choosensubject)
				{
					themenames = new string[subjectMap[i]._themeMap.Count];
					for (int j = 0; j < subjectMap[i]._themeMap.Count; j++)
					{
						themenames[j] = subjectMap[i]._themeMap[j]._name;
					}
				}
			}
			index = 0;
			return themenames;
		}

		private string GetQuestion()
		{
			string question = "no question available";
			for (int i = 0; i < subjectMap.Count; i++)
			{
				if (subjectMap[i]._subjectname == choosensubject)
				{
					for (int j = 0; j < subjectMap[i]._themeMap.Count; j++)
					{
						if (subjectMap[i]._themeMap[j]._name == choosentheme)
						{
							question = subjectMap[i]._themeMap[j]._question[index]._question;
							break;
						}
					}
				}
			}
			return question;
		}

		private string GetAnswer()
		{
			string answer = "no answer available";
			for (int i = 0; i < subjectMap.Count; i++)
			{
				if (subjectMap[i]._subjectname == choosensubject)
				{
					for (int j = 0; j < subjectMap[i]._themeMap.Count; j++)
					{
						if (subjectMap[i]._themeMap[j]._name == choosentheme)
						{
							answer = subjectMap[i]._themeMap[j]._question[index]._answer;

							break;
						}
					}
				}
			}
			return answer;
		}
		private void subjectBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			index = 0;
			activateThemeBox = false;
			choosensubject = subjectBox.SelectedItem.ToString();
			themeBox.ItemsSource = GetThemes();
			themeBox.SelectedItem = "";
			activateThemeBox = true;
		}

		private void themeBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			if (activateThemeBox) {
				onlyonce = true;
				rightanswer = 0;
				index = 0;
				choosentheme = themeBox.SelectedItem.ToString();
				questionLbl.Text = GetQuestion();
				for (int i = 0; i < subjectMap.Count; i++)
				{
					if (subjectMap[i]._subjectname == choosensubject)
					{
						for (int j = 0; j < subjectMap[i]._themeMap.Count; j++)
						{
							if (subjectMap[i]._themeMap[j]._name == choosentheme)
							{
								length = subjectMap[i]._themeMap[j]._question.Count;
							}
						}
					}
				} 
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			for (int i = 0; i < subjectMap.Count; i++)
			{
				if (subjectMap[i]._subjectname == choosensubject)
				{
					for (int j = 0; j < subjectMap[i]._themeMap.Count; j++)
					{
						if (index != (length - 1))
						{
							index++;
							choosensubject = subjectBox.SelectedItem.ToString();
							choosentheme = themeBox.SelectedItem.ToString();
							questionLbl.Text = GetQuestion();

							int percentage = index * 100 / length;

							percentLbl.Content = "You finished " + percentage + "%";

							break;
						}
						else
						{
							questionLbl.Text = "No questiones left, congratulations ;)";
							if (onlyonce)
							{
								int percentage = rightanswer * 100 / length;
								percentLbl.Content = "You got " + percentage + "%";
								onlyonce = false;
							}
							break;
						}
					}
				}
			}
		}

		private void Answer_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				choosensubject = subjectBox.SelectedItem.ToString();
				choosentheme = themeBox.SelectedItem.ToString();
				questionLbl.Text = GetAnswer();
			}
			catch (Exception)
			{
				MessageBox.Show("Please enter the subject and the theme first!");
			}
		}

		private void Right_Click(object sender, RoutedEventArgs e)
		{
			for (int i = 0; i < subjectMap.Count; i++)
			{
				if (subjectMap[i]._subjectname == choosensubject)
				{
					for (int j = 0; j < subjectMap[i]._themeMap.Count; j++)
					{
						if (index != (length - 1))
						{
							index++;
							choosensubject = subjectBox.SelectedItem.ToString();
							choosentheme = themeBox.SelectedItem.ToString();
							questionLbl.Text = GetQuestion();
							rightanswer++;
							
							int percentage = index * 100 / length;
							percentLbl.Content = "You finished " + percentage + "%";

							break;
						}
						else
						{
							questionLbl.Text = "No questiones left, congratulations ;)";
							if (onlyonce)
							{
								rightanswer++;
								int percentage = rightanswer * 100 / length;
								percentLbl.Content = "You got " + percentage + "%";
								onlyonce = false;
							}
							break;
						}
					}
				}
			}
		}
	}
}
