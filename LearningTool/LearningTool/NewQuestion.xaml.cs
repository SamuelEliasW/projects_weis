﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Lernprogram
{
	/// <summary>
	/// Interaktionslogik für UserControl1.xaml
	/// </summary>
	public partial class UserControl1 : Window
	{
		public List<Subject> newMap = MainWindow.subjectMap;
		static string FileName = @"Questions.xml";
		bool check = true;
		public UserControl1()
		{
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			check = true;
			for (int i = 0; i <= newMap.Count; i++)
			{
				if (i != newMap.Count && newMap[i]._subjectname == subjectLbl.Text)
				{
					for (int j = 0; j <= newMap[i]._themeMap.Count; j++)
					{
						if (j != newMap[i]._themeMap.Count && newMap[i]._themeMap[j]._name == themeLbl.Text  )
						{
							for (int k = 0; k <= newMap[i]._themeMap[j]._question.Count; k++)
							{
								if (k != newMap[i]._themeMap[j]._question.Count && newMap[i]._themeMap[j]._question[k]._question == questionLbl.Text )
								{
								}
								else
								{
									if (k == newMap[i]._themeMap[j]._question.Count && check)
									{
										newMap[i]._themeMap[j]._question.Add(new Text(questionLbl.Text, answerLbl.Text));
										check = false;
										break;
									}
								}
							}
						}
						else
						{
							if (j == newMap[i]._themeMap.Count && check)
							{
								newMap[i]._themeMap.Add(new Themes(themeLbl.Text, new List<Text> { new Text(questionLbl.Text, answerLbl.Text) }));
								check = false;
								break;
							}
						}
					}
				}
				else
				{
					if (i == newMap.Count && check)
					{
						newMap.Add(new Subject(new List<Themes> { new Themes(themeLbl.Text, new List<Text> { new Text(questionLbl.Text, answerLbl.Text) }) }, subjectLbl.Text));
						check = false;
						break;
					}
				}
			}

			SerializeItem(newMap);
		}


		private static void SerializeItem(object itemReferene)
		{
			Type t = itemReferene.GetType();
			XmlSerializer serializer = new XmlSerializer(t);
			TextWriter writer = new StreamWriter(FileName);

			serializer.Serialize(writer, itemReferene);
			writer.Close();
		}
	}
}
