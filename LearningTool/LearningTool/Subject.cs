﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lernprogram
{
	[Serializable]
	public class Subject
	{
		public List<Themes> _themeMap { get; set; }
		public string _subjectname { get; set; }
		public Subject(List<Themes> newlist, string newSubject)
		{
			_themeMap = newlist;
			_subjectname = newSubject;
		}
		public Subject() : this( new List<Themes>(), "")
		{

		}
	}
}
