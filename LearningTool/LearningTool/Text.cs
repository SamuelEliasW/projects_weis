﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lernprogram
{
	[Serializable]
	public class Text
	{
		public string _question { get; set; }
		public string _answer { get; set; }
		public Text(string question, string answer)
		{
			_question = question;
			_answer = answer;
		}

		public Text() : this("", "")
		{

		}
	}
}
