﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lernprogram
{
	[Serializable]
	public class Themes
	{
		public string _name { get; set; }
		private static Random rng = new Random();
		public List<Text> _question { get; set; }
		public Themes(string name, List<Text> question)
		{
			_name = name;
			_question = randomiseList(question);
		}

		public Themes() : this("", new List<Text>())
		{

		}

		private List<Text> randomiseList(List<Text> list)
		{
			int n = list.Count;
			while (n > 1)
			{
				n--;
				int k = rng.Next(n + 1);
				Text help = list[k];
				list[k] = list[n];
				list[n] = help;
			}
			return list;
		}
	}
}
