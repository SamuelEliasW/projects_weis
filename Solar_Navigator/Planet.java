package Solar_Navigator;

import Solar_Navigator.TimeCalculator.*;

public class Planet {

	private double degrees_;
	private double position_;
	private double mass_;
	private double BHA_;
	private double eccentricity_;
	private double diameter_;
	private double gravitation_;
	private double velocity_;
	private double sideralperiod_;
	private double brightness_;
	private double inclination_;
	private String name_;
	private int moon_;
	private int temperatur_;

	public Planet(String name, double degrees, double position, double mass, double BHA /*Big Half Axis*/,
			double eccentricity, double diameter, double gravitation, double velocity,
			double sideralperiod, double brightness, double inclination, int moon, int temperatur) {

		this.name_ = name;
		this.moon_ = moon;
		this.degrees_ = degrees;
		this.position_ = position;
		this.mass_ = mass;
		this.BHA_ = BHA;
		this.eccentricity_ = eccentricity;
		this.diameter_ = diameter;
		this.gravitation_ = gravitation;
		this.velocity_ = velocity;
		this.sideralperiod_ = sideralperiod;
		this.brightness_ = brightness;
		this.inclination_ = inclination;
		this.temperatur_ = temperatur;

	}


	public double elodiff(int first, int second, int planet, char side, TimeCalculator time) {
		//max elongation
		int venusL = -45;
		int venusR = 57;
		int mercuryL = -28;
		int mercuryR = 18;
		int counter = 0;
		if (planet == 1) {
			//checktime();
			if (side == 'L'){
				first -= mercuryL;
				if (first < 0) {
					first = first + 360;
				}
				while (first!= second) {
					if (first >= 360) {
						int fit = (int)first / 360;
						for (int i = 0; i < fit; i++) {
							first -= 360;
						}
					}
					first++;
					counter++;
				}
			}
			else {
				first += mercuryR;
				while (first!= second) {
					if (first >= 360) {
						int fit = (int)first / 360;
						for (int i = 0; i < fit; i++) {
							first -= 360;
						}
					}
					first++;
					counter++;
				}
			}
		}
		else if (planet == 2) {
			//checktime();
			if (side == 'L') {
				first -= venusL;
				while (first != second) {
					if (first >= 360) {
						int fit = (int)first / 360;
						for (int i = 0; i < fit; i++) {
							first -= 360;
						}
					}
					first++;
					counter++;
				}
			}
			else {
				first += venusR;
				if (first < 0) {
					first = first + 360;
				}
				while (first != second) {
					if (first >= 360) {
						int fit = (int)first / 360;
						for (int i = 0; i < fit; i++) {
							first -= 360;
						}
					}
					first++;
					counter++;
				}
			}
		}
		return counter;
	}






	void elongation(int planet, double degreesChangeEa,
			double degreesChangeMe, double degreesChangeVe, double MeDegrees, double EaDegrees, double VeDegrees,
			TimeCalculator pastdays) {


		double diffL;
		double diffR;
		double timeL;
		double timeR;
		double vdiff;


		if (planet == 1) { //mercury
			diffL = elodiff(degreesChangeMe, degreesChangeEa, degreesChangeVe, planet, 'L', pastdays);
			diffR = elodiff(degreesChangeMe, degreesChangeEa, degreesChangeVe, planet, 'R', pastdays);
			vdiff = MeDegrees - EaDegrees;
			timeL = diffL / vdiff;
			timeR = diffR / vdiff;
		}
		else if (planet == 2) { //venus
			diffL = elodiff(degreesChangeVe, degreesChangeEa, degreesChangeVe, planet, 'L', pastdays);
			diffR = elodiff(degreesChangeVe, degreesChangeEa, degreesChangeVe, planet, 'R', pastdays);
			vdiff = VeDegrees - EaDegrees;
			timeL = diffL / vdiff;
			timeR = diffR / vdiff;
		}
	}





	int opposition(int planet, double degreesChangeMe, double degreesChangeVe, double degreesChangeEa,
			double degreesChangeMa, double degreesChangeJu, double degreesChangeSa, double degreesChangeUr, double degreesChangeNe,
			double MeDegrees, double VeDegrees, double EaDegrees, double MaDegrees, double JuDegrees, double SaDegrees, double UrDegrees, double NeDegrees,
			TimeCalculator pastdays) {
		int p;
		double vdiff;
		int time = 0;
		if (planet == 1) {
			p = degreesdiff((int)(degreesChangeMe), (int)(degreesChangeEa));
			vdiff = MeDegrees - EaDegrees;
			time = (int) (p / vdiff);
		}
		else if (planet == 2) {
			p = degreesdiff((int)(degreesChangeVe), (int)(degreesChangeEa));
			vdiff = VeDegrees - EaDegrees;
			time = (int) (p / vdiff);
		}
		else if (planet == 4) {
			p = degreesdiff((int)(degreesChangeEa), (int)(degreesChangeMa));
			vdiff = EaDegrees - MaDegrees;
			time = (int) (p / vdiff);
		}
		else if (planet == 5) {
			p = degreesdiff((int)(degreesChangeEa), (int)(degreesChangeJu));
			vdiff = EaDegrees - JuDegrees;
			time = (int) (p / vdiff);
		}
		else if (planet == 6) {
			p = degreesdiff((int)(degreesChangeEa), (int)(degreesChangeSa));
			vdiff = EaDegrees - SaDegrees;
			time = (int) (p / vdiff);
		}
		else if (planet == 7) {
			p = degreesdiff((int)(degreesChangeEa), (int)(degreesChangeUr));
			vdiff = EaDegrees - UrDegrees;
			time = (int) (p / vdiff);
		}
		else if (planet == 8) {
			p = degreesdiff((int)(degreesChangeEa), (int)(degreesChangeNe));
			vdiff = EaDegrees - NeDegrees;
			time = (int) (p / vdiff);
		}
		return time;
	}




	int degreesdiff(int first, int second) {
		//checktime();
		int counter = 0;
		while (first != second) {
			//first & second Input is never over 360 degrees
			if (first >= 360) {
				first = -1;
			}
			first++;
			counter++;
		}
		return counter;
	}

	double elodiff(double degreesChangeMe, double degreesChangeEa,double degreesChangeVe, int planet, char side, TimeCalculator time) {
		//max elongation
		int venusL = -45;
		int venusR = 57;
		int mercuryL = -28;
		int mercuryR = 18;
		int counter = 0;
		if (planet == 1) {
			//checktime();
			if (side == 'L'){
				degreesChangeMe -= mercuryL;
				if (degreesChangeMe < 0) {
					degreesChangeMe = degreesChangeMe + 360;
				}
				while (degreesChangeMe!= degreesChangeEa) {
					if (degreesChangeMe >= 360) {
						int fit = (int)degreesChangeMe / 360;
						for (int i = 0; i < fit; i++) {
							degreesChangeMe -= 360;
						}
					}
					degreesChangeMe++;
					counter++;
				}
			}
			else {
				degreesChangeMe += mercuryR;
				while (degreesChangeMe!= degreesChangeEa) {
					if (degreesChangeMe >= 360) {
						int fit = (int)degreesChangeMe / 360;
						for (int i = 0; i < fit; i++) {
							degreesChangeMe -= 360;
						}
					}
					degreesChangeMe++;
					counter++;
				}
			}
		}
		else if (planet == 2) {
			//checktime();
			if (side == 'L') {
				degreesChangeVe -= venusL;
				while (degreesChangeVe != degreesChangeEa) {
					if (degreesChangeVe >= 360) {
						int fit = (int)degreesChangeVe / 360;
						for (int i = 0; i < fit; i++) {
							degreesChangeVe -= 360;
						}
					}
					degreesChangeVe++;
					counter++;
				}
			}
			else {
				degreesChangeVe += venusR;
				if (degreesChangeVe < 0) {
					degreesChangeVe = degreesChangeVe + 360;
				}
				while (degreesChangeVe != degreesChangeEa) {
					if (degreesChangeVe >= 360) {
						int fit = (int)degreesChangeVe / 360;
						for (int i = 0; i < fit; i++) {
							degreesChangeVe -= 360;
						}
					}
					degreesChangeVe++;
					counter++;
				}
			}
		}
		return counter;
	}



	public void printdata(double degreesChange) {

		System.out.println(name_);

		char [] arr = name_.toCharArray();
		String ret = "";

		for (int i = 0; i < arr.length; i++) {
			ret += "=";
		}
		System.out.println(ret);
		System.out.println("Position: " + degreesChange + " Degrees");
		System.out.println("Moons: " + moon_);
		System.out.println("Velocity: " + velocity_ + " km/s");
		System.out.println("Mass: " + mass_ + "kg");
		System.out.println("Big Half-axis: " + BHA_ + " AE");
		System.out.println("Eccentricity: " + eccentricity_);
		System.out.println("Equator Diameter: " + diameter_ + " km");
		System.out.println("Gravitational Acceleration: " + gravitation_ + " m/s^2");
		System.out.println("Sideral Obital Period: " + sideralperiod_ + " days");
		System.out.println("Inclination: " + inclination_ + " Degrees");
		if (brightness_ != 999){
			System.out.println("Apparent brightness: " + brightness_ + " mag");
		}
		System.out.println("Average Temperatur: " + temperatur_ + " degree centigrade");
		System.out.println();

	}

}
