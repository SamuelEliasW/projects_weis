package Solar_Navigator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

public class PosPlanets extends JPanel{
	private int diameter_;
	private double degrees_;
	private Color c_;
	private JPanel j_;
	private int range_;

	public PosPlanets(double degrees, int range, int diameter, Color c, JPanel j) {
		this.degrees_ = degrees;
		this.diameter_ = diameter;
		this.c_ = c;
		this.j_ = j;
		this.range_ = range;
	}
	

	@Override
	public void paintComponent(Graphics g) {
		int y = 0;
		int x = 0;

		y = (int) (-Math.sin(DegToRad(degrees_)) * range_) + j_.getHeight()/2;
		x = (int) (-Math.cos(DegToRad(degrees_)) * range_) + j_.getWidth()/2;


		Graphics2D outline = (Graphics2D) g;
		outline.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		outline.setColor(Color.WHITE);
		outline.drawOval(j_.getWidth()/2-range_, j_.getHeight()/2-range_, range_*2, range_*2);
		
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(c_);
		g2d.fillOval(x - diameter_/2, y - diameter_/2, diameter_, diameter_);


	}


	public double DegToRad(double deg) {
		double rad;

		rad = (deg * Math.PI)/180;

		return rad;
	}

	public void drawPanel() {
		Graphics2D g = (Graphics2D) j_.getGraphics();
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, j_.getWidth(), j_.getHeight());
	}


}
