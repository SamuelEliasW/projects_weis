package Solar_Navigator;

import java.lang.Math.*;
import java.util.Scanner;
import Solar_Navigator.TimeCalculator.*;
import Solar_Navigator.Planet.*;

//import org.omg.Messaging.SyncScopeHelper;

public class SolarNavigator {

	static Planet earth;
	static Planet mercury;
	static Planet venus;
	static Planet mars;
	static Planet jupiter;
	static Planet saturn;
	static Planet uranus;
	static Planet neptun;


	static double MeDegrees = 4.092350714456; //Mecurys Movement in a day in Degrees
	static double VeDegrees = 1.602129051495; //Venus Movement in a day in Degrees
	static double EaDegrees = 0.985610092647; //Earths Movement in a day in Degrees
	static double MaDegrees = 0.524037229323; //Mars Movement in a day in Degrees
	static double JuDegrees = 0.083140877598; //Jupiters Movement in a day in Degrees
	static double SaDegrees = 0.034329224125; //Saturns Movement in a day in Degrees
	static double UrDegrees = 0.011740145575; //Uranus Movement in a day in Degrees
	static double NeDegrees = 0.006136512865; //Neptuns Movement in a day in Degrees

	static double MeDeJan = 50.87352921;			//Mercurys Position on 1.1.2018
	static double VeDeJan = 173.2418274362;		//Venus Position on 1.1.2018
	static double EaDeJan = 0;					//Earths Position on 1.1.2018
	static double MaDeJan = 110.569405653;		//Mars Position on 1.1.2018
	static double JuDeJan = 228.25875690822;		//Jupiters Position on 1.1.2018
	static double SaDeJan = 169.5759197690248;	//Saturns Position on 1.1.2018
	static double UrDeJan = 289.54979051805445;	//Uranus Position on 1.1.2018
	static double NeDeJan = 306.1204002724133;	//Neptuns Position on 1.1.2018

	static double SuM = 1.9884 * Math.pow(10, 30);	//Suns Mass
	static double MeM = 3.301 * Math.pow(10, 23);		//Mercurys Mass
	static double VeM = 4.869 * Math.pow(10, 24);		//Venus Mass
	static double EaM = 5.974 * Math.pow(10, 24);		//Earths Mass
	static double MaM = 6.419 * Math.pow(10, 23);		//Mars Mass
	static double JuM = 1.899 * Math.pow(10, 27);		//Jupiters Mass
	static double SaM = 5.685 * Math.pow(10, 26);		//Saturns Mass
	static double UrM = 8.683 * Math.pow(10, 25);		//Uranus Mass
	static double NeM = 1.0243 * Math.pow(10, 26);	//Neptuns Mass

	static double MeBigHalfAxis = 0.387099273;	//Mercurys Big Half-axis in AE
	static double VeBigHalfAxis = 0.723;			//Venus Big Half-axis in AE
	static double EaBigHalfAxis = 1;				//Earths Big Half-axis in AE
	static double MaBigHalfAxis = 1.524;			//Mars Big Half-axis in AE
	static double JuBigHalfAxis = 5.203;			//Jupiter Big Half-axis in AE
	static double SaBigHalfAxis = 9.5826;		//Saturns Big Half-axis in AE
	static double UrBigHalfAxis = 19.201;		//Uranus Big Half-axis in AE
	static double NeBigHalfAxis = 30.070;		//Neptuns Big Half-axis in AE

	static double Gconst = 6.67408 * Math.pow(10, -11); //Gravitations Constant

	static double MeEc = 0.205635934;	//Mercurys eccentricity
	static double VeEc = 0.0067;			//Venus eccentricity
	static double EaEc = 0.0167;			//Earth eccentricity
	static double MaEc = 0.0935;			//Mars eccentricity
	static double JuEc = 0.0484;			//Jupiter eccentricity
	static double SaEc = 0.05648;		//Saturn eccentricity
	static double UrEc = 0.0472;			//Uranus eccentricity
	static double NeEc = 0.00859;		//Neptun eccentricity

	static double MeEqDi = 4879.4;		//Mercurys Equator-Diameter in km
	static double VeEqDi = 12103.6;		//Venus Equator-Diameter in km
	static double EaEqDi = 12756.32;		//Earths Equator-Diameter in km
	static double MaEqDi = 6792.4;		//Mars Equator-Diameter in km
	static double JuEqDi = 142984;		//Jupiters Equator-Diameter in km
	static double SaEqDi = 120536;		//Saturns Equator-Diameter in km
	static double UrEqDi = 51118;		//Uranus Equator-Diameter in km
	static double NeEqDi = 49528;		//Neptuns Equator-Diameter in km

	static double MeGrAc = 3.70;			//Mercurys gravitational acceleration in m/s^2
	static double VeGrAc = 8.87;			//Venus gravitational acceleration in m/s^2
	static double EaGrAc = 9.80665;		//Earth gravitational acceleration in m/s^2
	static double MaGrAc = 3.69;			//Mars gravitational acceleration in m/s^2
	static double JuGrAc = 24.79;		//Jupiters gravitational acceleration in m/s^2
	static double SaGrAc = 10.44;		//Saturns gravitational acceleration in m/s^2
	static double UrGrAc = 8.87;			//Urnanus gravitational acceleration in m/s^2
	static double NeGrAc = 11.15;		//Neptuns gravitational acceleration in m/s^2

	static double MeV = 47.36;	//Mercurys velocity in km/s
	static double VeV = 35.02;	//Venus velocity in km/s
	static double EaV = 29.78;	//Earths velocity in km/s
	static double MaV = 24.13;	//Mars velocity in km/s
	static double JuV = 13.07;	//Jupiters velocity in km/s
	static double SaV = 9.69;	//Saturns velocity in km/s
	static double UrV = 6.81;	//Uranus velocity in km/s
	static double NeV = 5.43;	//Neptuns velocity in km/s

	static double MeD = 87.969;		//Mercurys Sidereal orbital period in days
	static double VeD = 224.701;		//Venus Sidereal orbital period in days
	static double EaD = 365.256;		//Earths Sidereal orbital period in days
	static double MaD = 686.980;		//Mars Sidereal orbital period in days
	static double JuD = 4332.816;		//Jupiters Sidereal orbital period in days
	static double SaD = 10494.232992;	//Saturns Sidereal orbital period in days
	static 	double UrD = 30685.521816;	//Uranus Sidereal orbital period in days
	static 	double NeD = 60190.53624;	//Neptun Sidereal orbital period in days

	static 	double MeAB = -1.9;	//Mercurys Apparent brightness
	static 	double VeAB = -4.6;	//Venus Apparent brightness
	static 	double MaAB = -2.91;	//Mars Apparent brightness
	static 	double JuAB = -2.94;	//Jupiters Apparent brightness
	static 	double SaAB = 0.43;	//Saturns Apparent brightness
	static 	double UrAB = 5.32;	//Uranus Apparent brightness
	static 	double NeAB = 7.78;	//Neptuns Apparent brightness

	static 	double MeI = 7.00487;	//Mercurys Inclination
	static 	double VeI = 3.39471;	//Venus Inclination
	static 	double EaI = 0;		//Earths Inclination
	static 	double MaI = 1.85061;	//Mars Inclination
	static 	double JuI = 1.30530;	//Jupiters Inclination
	static 	double SaI = 2.48446;	//Satunrs Inclination
	static 	double UrI = 0.76986;	//Uranus Inclination
	static 	double NeI = 1.76917;	//Neptuns Inclination


	static double degreesChangeMe;
	static double degreesChangeVe;
	static double degreesChangeEa;
	static double degreesChangeMa;
	static 	double degreesChangeJu;
	static 	double degreesChangeSa;
	static 	double degreesChangeUr;
	static 	double degreesChangeNe;

	static TimeCalculator date;


	public static void main(String[] args) {




		double YourW; //Your Weight
		double YourM; //Your Mass
		int input;
		int Day;
		int month;
		int year;
		double time;	 //Time since the 1 August 2018

		int close = 0;

		//elongation Left-Side & Right-Side
		double timeL;
		double timeR;

		Scanner read = new Scanner(System.in);

		System.out.println("Solar Navigator");
		System.out.println("===============");
		System.out.println("This Programm will deliver positions and informations about the Planets");
		System.out.println("in our solarystem");

		System.out.println("Navigator,Positions and Informations: 1");
		System.out.println("Your Weight on other planets: 2");
		System.out.println("Next Opposition: 3");

		input = read.nextInt();
		System.out.println();

		if (input == 1) {
			System.out.println("Solar System Navigator");
			System.out.println("======================");
			System.out.println();
			System.out.println("Please put the Day of the Time you want to read the Positions: ");
			Day = read.nextInt();
			System.out.println("Please put the Month of the Time you want to read the positions: ");
			month = read.nextInt();
			System.out.println("Please put the Year of the Time you want to read the Positions: ");

			year = read.nextInt();

			date = new TimeCalculator(Day, month, year);

			time = date.gettime();

			System.out.println("Please write the Planet you want to know more about :");
			System.out.println();
			while (close != 1) {
				System.out.println("Mercury = 1");
				System.out.println("Venus = 2");
				System.out.println("Earth = 3");
				System.out.println("Mars = 4");
				System.out.println("Jupiter = 5");
				System.out.println("Saturn = 6");
				System.out.println("Uranus = 7");
				System.out.println("Neptun = 8");

				int planetchosen = read.nextInt();

				System.out.println();

				checktime();

				createPlanets();


				if (planetchosen == 1) { //Mercury
					mercury.printdata(degreesChangeMe);

				}

				if (planetchosen == 2) { //Venus
					venus.printdata(degreesChangeVe);

				}

				if (planetchosen == 3) { //Earth
					earth.printdata(degreesChangeEa);

				}

				if (planetchosen == 4) { //Mars
					mars.printdata(degreesChangeMa);

				}

				if (planetchosen == 5) { //Jupiter
					jupiter.printdata(degreesChangeJu);

				}
				if (planetchosen == 6) { //Saturn
					saturn.printdata(degreesChangeSa);

				}
				if (planetchosen == 7) { //Uranus
					uranus.printdata(degreesChangeUr);

				}
				if (planetchosen == 8) { //Neptun
					neptun.printdata(degreesChangeNe);

				}


				System.out.println("Do you want to close the Programm? yes = 1, no = 0");
				close = read.nextInt();
			}
		}
		else if (input == 2) {
			System.out.println("Please write your Weight in kg :");
			YourW = read.nextDouble();

			YourM = YourW / EaGrAc;

			System.out.println("On Mercury you would been " + YourM * MeGrAc + " kg");
			System.out.println("On Venus you would been " + YourM * VeGrAc + " kg");
			System.out.println("On Earth you would been " + YourM * EaGrAc + " kg");
			System.out.println("On Mars you would been " + YourM * MaGrAc + " kg");
			System.out.println("On Jupiter you would been " + YourM * JuGrAc + " kg");
			System.out.println("On Saturn you would been " + YourM * SaGrAc + " kg");
			System.out.println("On Uranus you would been " + YourM * UrGrAc + " kg");
			System.out.println("On Neptun you would been " + YourM * NeGrAc + " kg");
		}
		else if (input == 3) {
			System.out.println("Oppositions and Elongations");
			System.out.println("===========================");
			System.out.println("Please write today's day: ");
			Day = read.nextInt();
			System.out.println("Please write today's month: ");
			month = read.nextInt();
			System.out.println("Please write today's year: ");
			year = read.nextInt();

			date = new TimeCalculator(Day, month, year);

			time = date.gettime();

			checktime();

			createPlanets();


			System.out.println("Please type the number in:");
			while (close != 1) {
				System.out.println("Mercury = 1");
				System.out.println("Venus = 2");
				System.out.println("Mars = 4");
				System.out.println("Jupiter = 5");
				System.out.println("Saturn = 6");
				System.out.println("Uranus = 7");
				System.out.println("Neptun = 8");
				int planet = read.nextInt();
				double newtime;


				if (planet == 1) {
					//Elongation Right Side (from Earth to Sun) 28 Degree
					degreesChangeMe += 28;

					if (degreesChangeMe >= 360) {
						int fit = (int)degreesChangeMe / 360;
						for (int i = 0; i < fit; i++) {
							degreesChangeMe -= 360;
						}
					}
					int leftnewtime = mars.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa,
							degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(leftnewtime + " days");


					degreesChangeMe -= 28 * 2;

					if (degreesChangeMe < 0) {
						degreesChangeMe = 360 + degreesChangeMe;
					}
					//Elongation Left Side (from Earth to Sun) 28 Degree
					int rightnewtime = mars.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa,
							degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(rightnewtime + " days");
				}
				else if (planet == 2) {
					degreesChangeVe += 47;

					if (degreesChangeVe >= 360) {
						int fit = (int) degreesChangeVe / 360;
						for (int i = 0; i < fit; i++) {
							degreesChangeVe -= 360;
						}
					}

					//Elongation Right Side (from Earth to Sun) 47 Degree
					int leftnewtime = mars.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa,
							degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(leftnewtime + " days");


					degreesChangeVe -= 47;

					if (degreesChangeVe < 0) {
						degreesChangeVe = 360 + degreesChangeVe;
					}
					//Elongation Left Side (from Earth to Sun) 47 Degree
					int rightnewtime = mars.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa,
							degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(rightnewtime + " days");
				}
				else if (planet == 3) {
					System.out.println("???");
				}
				else if (planet == 4) {
					newtime = mars.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa,
							degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(newtime + " days");
				}
				else if (planet == 5) {
					newtime = jupiter.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa, degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(newtime + " days");
				}
				else if (planet == 6) {
					newtime = saturn.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa, degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(newtime + " days");
				}
				else if (planet == 7) {
					newtime = uranus.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa, degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(newtime + " days");
				}
				else if (planet == 8) {
					newtime = neptun.opposition(planet, degreesChangeMe, degreesChangeVe, degreesChangeEa, degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
							MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
							date);
					System.out.println(newtime + " days");
				}
				System.out.println("Do you want to close the Programm? yes = 1, no = 0");
				close = read.nextInt();
			}
		}
	}

	public static void checktime() {
		int divider;
		degreesChangeMe = (date.gettime() * MeDegrees) + MeDeJan;
		degreesChangeVe = (date.gettime() * VeDegrees) + VeDeJan;
		degreesChangeEa = (date.gettime() * EaDegrees) + EaDeJan;
		degreesChangeMa = (date.gettime() * MaDegrees) + MaDeJan;
		degreesChangeJu = (date.gettime() * JuDegrees) + JuDeJan;
		degreesChangeSa = (date.gettime() * SaDegrees) + SaDeJan;
		degreesChangeUr = (date.gettime() * UrDegrees) + UrDeJan;
		degreesChangeNe = (date.gettime() * NeDegrees) + NeDeJan;

		if (degreesChangeMe < 0) {
			divider = (int) (-degreesChangeMe / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeMe = (divider * 360 - degreesChangeMe) + degreesChangeMe;
		}

		if (degreesChangeVe < 0) {
			divider = (int) (-degreesChangeVe / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeVe = (divider * 360 - degreesChangeVe) + degreesChangeVe;
		}

		if (degreesChangeEa < 0) {
			divider = (int) (-degreesChangeEa / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeEa = (divider * 360 - degreesChangeEa) + degreesChangeEa;
		}

		if (degreesChangeMa < 0) {
			divider = (int) (-degreesChangeMa / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeMa = (divider * 360 - degreesChangeMa) + degreesChangeMa;
		}

		if (degreesChangeJu < 0) {
			divider = (int) (-degreesChangeJu / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeJu = (divider * 360 - degreesChangeJu) + degreesChangeJu;
		}

		if (degreesChangeSa < 0) {
			divider = (int) (-degreesChangeSa / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeSa = (divider * 360 - degreesChangeSa) + degreesChangeSa;
		}

		if (degreesChangeUr < 0) {
			divider = (int) (-degreesChangeUr / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeUr = (divider * 360  - degreesChangeUr) + degreesChangeUr;
		}

		if (degreesChangeNe < 0) {
			divider = (int) (-degreesChangeNe / 360);
			//divider * 360, because you don't need 1.002, just 1
			degreesChangeNe = (divider * 360 - degreesChangeNe) + degreesChangeNe;
		}

		if (degreesChangeVe >= 360) {
			int fit = (int) degreesChangeVe / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeVe -= 360;
			}
		}
		if (degreesChangeMe >= 360) {
			int fit = (int)degreesChangeMe / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeMe -= 360;
			}
		}
		if (degreesChangeEa >= 360) {
			int fit = (int)degreesChangeEa / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeEa -= 360;
			}
		}
		if (degreesChangeMa >= 360) {
			int fit = (int)degreesChangeMa / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeMa -= 360;
			}
		}
		if (degreesChangeJu >= 360) {
			int fit = (int)degreesChangeJu / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeJu -= 360;
			}
		}
		if (degreesChangeSa >= 360) {
			int fit = (int)degreesChangeSa / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeSa -= 360;
			}
		}
		if (degreesChangeUr >= 360) {
			int fit = (int)degreesChangeUr / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeUr -= 360;
			}
		}
		if (degreesChangeNe >= 360) {
			int fit = (int)degreesChangeNe / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeNe -= 360;
			}
		}
	}

	public static void createPlanets() {
		mercury= new Planet("Mercury", degreesChangeMe, MeDeJan, MeM, MeBigHalfAxis,
				MeEc, MeEqDi, MeGrAc, MeV, MeD, MeAB, MeI, 0, 167);
		venus = new Planet("Venus", degreesChangeVe, VeDeJan, VeM, VeBigHalfAxis,
				VeEc, VeEqDi, VeGrAc, VeV, VeD, VeAB, VeI, 0, 464);
		earth = new Planet("Earth", degreesChangeEa, EaDeJan, EaM, EaBigHalfAxis,
				EaEc, EaEqDi, EaGrAc, EaV, EaD, 999, EaI, 1, 15);
		mars = new Planet("Mars", degreesChangeMa, MaDeJan, MaM, MaBigHalfAxis,
				MaEc, MaEqDi, MaGrAc, MaV, MaD, MaAB, MaI, 2, -55);
		jupiter = new Planet("Jupiter", degreesChangeJu, JuDeJan, JuM, JuBigHalfAxis,
				JuEc, JuEqDi, JuGrAc, JuV, JuD, JuAB, JuI, 79, -108);
		saturn = new Planet("Satrun", degreesChangeSa, SaDeJan, SaM, SaBigHalfAxis,
				SaEc, SaEqDi, SaGrAc, SaV, SaD, SaAB, SaI, 62, -139);
		uranus = new Planet("Uranus", degreesChangeUr, UrDeJan, UrM, UrBigHalfAxis,
				UrEc, UrEqDi, UrGrAc, UrV, UrD, UrAB, UrI, 27, -197);
		neptun = new Planet("Neptun", degreesChangeNe, NeDeJan, NeM, NeBigHalfAxis,
				NeEc, NeEqDi, NeGrAc, NeV, NeD, NeAB, NeI, 14, -201);
	}
}


