//Samuel Weissenbacher
//This Programm calculates and draws the paths of all 8 Planets of our Sol-System


package Solar_Navigator;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JTextField;
import javax.swing.JWindow;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.lang.Math.*;
import java.util.Scanner;
import Solar_Navigator.TimeCalculator.*;
import Solar_Navigator.Planet.*;
import Solar_Navigator.PosPlanets.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JSlider;

public class SolarNavigatorGUI extends JFrame {

	private JPanel contentPane;
	private JLabel lblSolarNavigator;
	private JLabel lblBySamuelWeissenbacher;
	private static JPanel panel;
	private JLabel lblDay;
	private JLabel lblMonth;
	private JLabel lblYear;
	private static JTextField TxtDay;
	private static JTextField TxtMonth;
	private static JTextField TxtYear;
	private static JButton btnStart;

	//global variables


	static Planet earth;
	static Planet mercury;
	static Planet venus;
	static Planet mars;
	static Planet jupiter;
	static Planet saturn;
	static Planet uranus;
	static Planet neptun;


	static double MeDegrees = 4.092350714456; //Mecurys Movement in a day in Degrees
	static double VeDegrees = 1.602129051495; //Venus Movement in a day in Degrees
	static double EaDegrees = 0.985610092647; //Earths Movement in a day in Degrees
	static double MaDegrees = 0.524037229323; //Mars Movement in a day in Degrees
	static double JuDegrees = 0.083140877598; //Jupiters Movement in a day in Degrees
	static double SaDegrees = 0.034329224125; //Saturns Movement in a day in Degrees
	static double UrDegrees = 0.011740145575; //Uranus Movement in a day in Degrees
	static double NeDegrees = 0.006136512865; //Neptuns Movement in a day in Degrees

	static double MeDeJan = 50.87352921;			//Mercurys Position on 1.1.2018
	static double VeDeJan = 173.2418274362;		//Venus Position on 1.1.2018
	static double EaDeJan = 0;					//Earths Position on 1.1.2018
	static double MaDeJan = 110.569405653;		//Mars Position on 1.1.2018
	static double JuDeJan = 228.25875690822;		//Jupiters Position on 1.1.2018
	static double SaDeJan = 169.5759197690248;	//Saturns Position on 1.1.2018
	static double UrDeJan = 289.54979051805445;	//Uranus Position on 1.1.2018
	static double NeDeJan = 306.1204002724133;	//Neptuns Position on 1.1.2018

	static double SuM = 1.9884 * Math.pow(10, 30);	//Suns Mass
	static double MeM = 3.301 * Math.pow(10, 23);		//Mercurys Mass
	static double VeM = 4.869 * Math.pow(10, 24);		//Venus Mass
	static double EaM = 5.974 * Math.pow(10, 24);		//Earths Mass
	static double MaM = 6.419 * Math.pow(10, 23);		//Mars Mass
	static double JuM = 1.899 * Math.pow(10, 27);		//Jupiters Mass
	static double SaM = 5.685 * Math.pow(10, 26);		//Saturns Mass
	static double UrM = 8.683 * Math.pow(10, 25);		//Uranus Mass
	static double NeM = 1.0243 * Math.pow(10, 26);	//Neptuns Mass

	static double MeBigHalfAxis = 0.387099273;	//Mercurys Big Half-axis in AE
	static double VeBigHalfAxis = 0.723;			//Venus Big Half-axis in AE
	static double EaBigHalfAxis = 1;				//Earths Big Half-axis in AE
	static double MaBigHalfAxis = 1.524;			//Mars Big Half-axis in AE
	static double JuBigHalfAxis = 5.203;			//Jupiter Big Half-axis in AE
	static double SaBigHalfAxis = 9.5826;		//Saturns Big Half-axis in AE
	static double UrBigHalfAxis = 19.201;		//Uranus Big Half-axis in AE
	static double NeBigHalfAxis = 30.070;		//Neptuns Big Half-axis in AE

	static double Gconst = 6.67408 * Math.pow(10, -11); //Gravitations Constant

	static double MeEc = 0.205635934;	//Mercurys eccentricity
	static double VeEc = 0.0067;			//Venus eccentricity
	static double EaEc = 0.0167;			//Earth eccentricity
	static double MaEc = 0.0935;			//Mars eccentricity
	static double JuEc = 0.0484;			//Jupiter eccentricity
	static double SaEc = 0.05648;		//Saturn eccentricity
	static double UrEc = 0.0472;			//Uranus eccentricity
	static double NeEc = 0.00859;		//Neptun eccentricity

	static double MeEqDi = 4879.4;		//Mercurys Equator-Diameter in km
	static double VeEqDi = 12103.6;		//Venus Equator-Diameter in km
	static double EaEqDi = 12756.32;		//Earths Equator-Diameter in km
	static double MaEqDi = 6792.4;		//Mars Equator-Diameter in km
	static double JuEqDi = 142984;		//Jupiters Equator-Diameter in km
	static double SaEqDi = 120536;		//Saturns Equator-Diameter in km
	static double UrEqDi = 51118;		//Uranus Equator-Diameter in km
	static double NeEqDi = 49528;		//Neptuns Equator-Diameter in km

	static double MeGrAc = 3.70;			//Mercurys gravitational acceleration in m/s^2
	static double VeGrAc = 8.87;			//Venus gravitational acceleration in m/s^2
	static double EaGrAc = 9.80665;		//Earth gravitational acceleration in m/s^2
	static double MaGrAc = 3.69;			//Mars gravitational acceleration in m/s^2
	static double JuGrAc = 24.79;		//Jupiters gravitational acceleration in m/s^2
	static double SaGrAc = 10.44;		//Saturns gravitational acceleration in m/s^2
	static double UrGrAc = 8.87;			//Urnanus gravitational acceleration in m/s^2
	static double NeGrAc = 11.15;		//Neptuns gravitational acceleration in m/s^2

	static double MeV = 47.36;	//Mercurys velocity in km/s
	static double VeV = 35.02;	//Venus velocity in km/s
	static double EaV = 29.78;	//Earths velocity in km/s
	static double MaV = 24.13;	//Mars velocity in km/s
	static double JuV = 13.07;	//Jupiters velocity in km/s
	static double SaV = 9.69;	//Saturns velocity in km/s
	static double UrV = 6.81;	//Uranus velocity in km/s
	static double NeV = 5.43;	//Neptuns velocity in km/s

	static double MeD = 87.969;		//Mercurys Sidereal orbital period in days
	static double VeD = 224.701;		//Venus Sidereal orbital period in days
	static double EaD = 365.256;		//Earths Sidereal orbital period in days
	static double MaD = 686.980;		//Mars Sidereal orbital period in days
	static double JuD = 4332.816;		//Jupiters Sidereal orbital period in days
	static double SaD = 10494.232992;	//Saturns Sidereal orbital period in days
	static 	double UrD = 30685.521816;	//Uranus Sidereal orbital period in days
	static 	double NeD = 60190.53624;	//Neptun Sidereal orbital period in days

	static 	double MeAB = -1.9;	//Mercurys Apparent brightness
	static 	double VeAB = -4.6;	//Venus Apparent brightness
	static 	double MaAB = -2.91;	//Mars Apparent brightness
	static 	double JuAB = -2.94;	//Jupiters Apparent brightness
	static 	double SaAB = 0.43;	//Saturns Apparent brightness
	static 	double UrAB = 5.32;	//Uranus Apparent brightness
	static 	double NeAB = 7.78;	//Neptuns Apparent brightness

	static 	double MeI = 7.00487;	//Mercurys Inclination
	static 	double VeI = 3.39471;	//Venus Inclination
	static 	double EaI = 0;		//Earths Inclination
	static 	double MaI = 1.85061;	//Mars Inclination
	static 	double JuI = 1.30530;	//Jupiters Inclination
	static 	double SaI = 2.48446;	//Satunrs Inclination
	static 	double UrI = 0.76986;	//Uranus Inclination
	static 	double NeI = 1.76917;	//Neptuns Inclination
	
	static 	double MeT = 440;	//Average Temperatur
	static 	double VeT = 737;	
	static 	double EaT = 288;		
	static 	double MaT = 218;	
	static 	double JuT = 165;	
	static 	double SaT = 134;	
	static 	double UrT = 76;	
	static 	double NeT = 72;


	static double degreesChangeMe;
	static double degreesChangeVe;
	static double degreesChangeEa;
	static double degreesChangeMa;
	static double degreesChangeJu;
	static double degreesChangeSa;
	static double degreesChangeUr;
	static double degreesChangeNe;

	static TimeCalculator date;
	private JSlider slider;
	private JButton btnStartSimulation;
	private JLabel lblDayssec;
	private JButton btnVenus;
	private JButton btnMars;
	private JButton btnMercury;
	private JButton btnEarth;
	private JButton btnSun;
	private JButton btnJupiter;
	private JButton btnSaturn;
	private JButton btnUranus;
	private JButton btnNeptun;
	private JLabel lblInfos;

	//Planet Gui
	JFrame PlanetGUI;
	JLabel planetInfo;

	private JPanel panelPlanets;
	Image image;

	//Path pictures
	String path = "";
	private JLabel lblSelectedPlanet;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SolarNavigatorGUI frame = new SolarNavigatorGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SolarNavigatorGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 514, 483);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setForeground(Color.WHITE);
		contentPane.setName("SolarNavigator");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblSolarNavigator());
		contentPane.add(getLblBySamuelWeissenbacher());
		contentPane.add(getPanel());
		contentPane.add(getLblDay());
		contentPane.add(getLblMonth());
		contentPane.add(getLblYear());
		contentPane.add(getTxtDay());
		contentPane.add(getTxtMonth());
		contentPane.add(getTxtYear());
		contentPane.add(getBtnStart());
		contentPane.add(getSlider());
		contentPane.add(getBtnStartSimulation());
		contentPane.add(getLblDayssec());
		contentPane.add(getBtnVenus());
		contentPane.add(getBtnMars());
		contentPane.add(getBtnMercury());
		contentPane.add(getBtnEarth());
		contentPane.add(getBtnSun());
		contentPane.add(getBtnJupiter());
		contentPane.add(getBtnSaturn());
		contentPane.add(getBtnUranus());
		contentPane.add(getBtnNeptun());
		contentPane.add(getLblInfos());
		contentPane.add(getLblSelectedPlanet());
	}
	public JLabel getLblSolarNavigator() {
		if (lblSolarNavigator == null) {
			lblSolarNavigator = new JLabel("Solar Navigator");
			lblSolarNavigator.setBorder(new LineBorder(Color.WHITE));
			lblSolarNavigator.setForeground(Color.WHITE);
			lblSolarNavigator.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblSolarNavigator.setHorizontalAlignment(SwingConstants.CENTER);
			lblSolarNavigator.setBounds(10, 11, 140, 25);
		}
		return lblSolarNavigator;
	}
	public JLabel getLblBySamuelWeissenbacher() {
		if (lblBySamuelWeissenbacher == null) {
			lblBySamuelWeissenbacher = new JLabel("by Samuel Wei\u00DFenbacher, 3BHEL, 2019");
			lblBySamuelWeissenbacher.setForeground(Color.GRAY);
			lblBySamuelWeissenbacher.setBounds(166, 362, 240, 14);
		}
		return lblBySamuelWeissenbacher;
	}
	public JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new LineBorder(Color.GRAY));
			panel.setBackground(Color.BLACK);
			panel.setForeground(Color.WHITE);
			panel.setBounds(10, 40, 384, 306);
		}
		return panel;
	}
	public JLabel getLblDay() {
		if (lblDay == null) {
			lblDay = new JLabel("Day:");
			lblDay.setForeground(Color.WHITE);
			lblDay.setBounds(10, 346, 46, 14);
		}
		return lblDay;
	}
	public JLabel getLblMonth() {
		if (lblMonth == null) {
			lblMonth = new JLabel("Month:");
			lblMonth.setForeground(Color.WHITE);
			lblMonth.setBounds(50, 346, 46, 14);
		}
		return lblMonth;
	}
	public JLabel getLblYear() {
		if (lblYear == null) {
			lblYear = new JLabel("Year:");
			lblYear.setForeground(Color.WHITE);
			lblYear.setBounds(106, 346, 46, 14);
		}
		return lblYear;
	}
	public JTextField getTxtDay() {
		if (TxtDay == null) {
			TxtDay = new JTextField();
			TxtDay.setText("1");
			TxtDay.setBounds(10, 360, 30, 20);
			TxtDay.setColumns(10);
		}
		return TxtDay;
	}
	public JTextField getTxtMonth() {
		if (TxtMonth == null) {
			TxtMonth = new JTextField();
			TxtMonth.setText("1");
			TxtMonth.setBounds(50, 360, 46, 20);
			TxtMonth.setColumns(10);
		}
		return TxtMonth;
	}
	public JTextField getTxtYear() {
		if (TxtYear == null) {
			TxtYear = new JTextField();
			TxtYear.setText("2018");
			TxtYear.setBounds(106, 360, 50, 20);
			TxtYear.setColumns(10);
		}
		return TxtYear;
	}
	public JButton getBtnStart() {
		if (btnStart == null) {
			btnStart = new JButton("Calculate Date");
			btnStart.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					startProgramm();

				}
			});
			btnStart.setBounds(260, 14, 134, 23);
		}
		return btnStart;
	}

	public static void startProgramm() {
		int day = Integer.parseInt(TxtDay.getText());
		int month = Integer.parseInt(TxtMonth.getText());
		int year = Integer.parseInt(TxtYear.getText());

		date = new TimeCalculator(day, month, year);

		checktime();

		createPlanets();

		drawPlanets();
	}


	public static void drawPlanets() {

		Color JuColor = new Color(210, 105, 30);
		Color SaColor = new Color(139, 0, 0);
		Color UrColor = new Color(135, 206, 250);
		Color NeColor = new Color(0, 0, 139);

		PosPlanets Sun = new PosPlanets(0, 0, 21,Color.YELLOW, panel);
		PosPlanets Mercury = new PosPlanets(degreesChangeMe, 35, 8,Color.LIGHT_GRAY, panel);
		PosPlanets Venus = new PosPlanets(degreesChangeVe, 45, 9,Color.ORANGE, panel);
		PosPlanets Earth = new PosPlanets(degreesChangeEa, 60, 10,Color.BLUE, panel);
		PosPlanets Mars = new PosPlanets(degreesChangeMa, 80, 9,Color.RED, panel);
		PosPlanets Jupiter = new PosPlanets(degreesChangeJu, 95, 13, JuColor, panel);
		PosPlanets Saturn = new PosPlanets(degreesChangeSa, 110, 12, SaColor, panel);
		PosPlanets Uranus = new PosPlanets(degreesChangeUr, 125, 12, UrColor, panel);
		PosPlanets Neptun = new PosPlanets(degreesChangeNe, 145, 12, NeColor, panel);

		Sun.drawPanel();

		Sun.paintComponent(panel.getGraphics());
		Mercury.paintComponent(panel.getGraphics());
		Venus.paintComponent(panel.getGraphics());
		Earth.paintComponent(panel.getGraphics());
		Mars.paintComponent(panel.getGraphics());
		Jupiter.paintComponent(panel.getGraphics());
		Saturn.paintComponent(panel.getGraphics());
		Uranus.paintComponent(panel.getGraphics());
		Neptun.paintComponent(panel.getGraphics());

	}


	public static void checktime() {
		degreesChangeMe = (date.gettime() * MeDegrees) + MeDeJan;
		degreesChangeVe = (date.gettime() * VeDegrees) + VeDeJan;
		degreesChangeEa = (date.gettime() * EaDegrees) + EaDeJan;
		degreesChangeMa = (date.gettime() * MaDegrees) + MaDeJan;
		degreesChangeJu = (date.gettime() * JuDegrees) + JuDeJan;
		degreesChangeSa = (date.gettime() * SaDegrees) + SaDeJan;
		degreesChangeUr = (date.gettime() * UrDegrees) + UrDeJan;
		degreesChangeNe = (date.gettime() * NeDegrees) + NeDeJan;

		if (degreesChangeMe < 0) {
			int fit = (int) (-degreesChangeMe / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeMe += 360;
			}
		}

		if (degreesChangeVe < 0) {
			int fit = (int) (-degreesChangeVe / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeVe += 360;
			}
		}

		if (degreesChangeEa < 0) {
			int fit = (int) (-degreesChangeEa / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeEa += 360;
			}
		}

		if (degreesChangeMa < 0) {
			int fit = (int) (-degreesChangeMa / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeMa += 360;
			}
		}

		if (degreesChangeJu < 0) {
			int fit = (int) (-degreesChangeJu / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeJu += 360;
			}
		}

		if (degreesChangeSa < 0) {
			int fit = (int) (-degreesChangeSa / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeSa += 360;
			}
		}

		if (degreesChangeUr < 0) {
			int fit = (int) (-degreesChangeUr / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeUr += 360;
			}
		}

		if (degreesChangeNe < 0) {
			int fit = (int) (-degreesChangeNe / 360);
			for (int i = 0; i < fit; i++) {
				degreesChangeNe += 360;
			}
		}

		if (degreesChangeVe >= 360) {
			int fit = (int) degreesChangeVe / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeVe -= 360;
			}
		}
		if (degreesChangeMe >= 360) {
			int fit = (int)degreesChangeMe / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeMe -= 360;
			}
		}
		if (degreesChangeEa >= 360) {
			int fit = (int)degreesChangeEa / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeEa -= 360;
			}
		}
		if (degreesChangeMa >= 360) {
			int fit = (int)degreesChangeMa / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeMa -= 360;
			}
		}
		if (degreesChangeJu >= 360) {
			int fit = (int)degreesChangeJu / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeJu -= 360;
			}
		}
		if (degreesChangeSa >= 360) {
			int fit = (int)degreesChangeSa / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeSa -= 360;
			}
		}
		if (degreesChangeUr >= 360) {
			int fit = (int)degreesChangeUr / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeUr -= 360;
			}
		}
		if (degreesChangeNe >= 360) {
			int fit = (int)degreesChangeNe / 360;
			for (int i = 0; i < fit; i++) {
				degreesChangeNe -= 360;
			}
		}
	}

	public static void createPlanets() {
		mercury= new Planet("Mercury", degreesChangeMe, MeDeJan, MeM, MeBigHalfAxis,
				MeEc, MeEqDi, MeGrAc, MeV, MeD, MeAB, MeI, 0, 167);
		venus = new Planet("Venus", degreesChangeVe, VeDeJan, VeM, VeBigHalfAxis,
				VeEc, VeEqDi, VeGrAc, VeV, VeD, VeAB, VeI, 0, 464);
		earth = new Planet("Earth", degreesChangeEa, EaDeJan, EaM, EaBigHalfAxis,
				EaEc, EaEqDi, EaGrAc, EaV, EaD, 999, EaI, 1, 15);
		mars = new Planet("Mars", degreesChangeMa, MaDeJan, MaM, MaBigHalfAxis,
				MaEc, MaEqDi, MaGrAc, MaV, MaD, MaAB, MaI, 2, -55);
		jupiter = new Planet("Jupiter", degreesChangeJu, JuDeJan, JuM, JuBigHalfAxis,
				JuEc, JuEqDi, JuGrAc, JuV, JuD, JuAB, JuI, 79, -108);
		saturn = new Planet("Satrun", degreesChangeSa, SaDeJan, SaM, SaBigHalfAxis,
				SaEc, SaEqDi, SaGrAc, SaV, SaD, SaAB, SaI, 62, -139);
		uranus = new Planet("Uranus", degreesChangeUr, UrDeJan, UrM, UrBigHalfAxis,
				UrEc, UrEqDi, UrGrAc, UrV, UrD, UrAB, UrI, 27, -197);
		neptun = new Planet("Neptun", degreesChangeNe, NeDeJan, NeM, NeBigHalfAxis,
				NeEc, NeEqDi, NeGrAc, NeV, NeD, NeAB, NeI, 14, -201);
	}
	public JSlider getSlider() {
		if (slider == null) {
			slider = new JSlider();
			slider.setValue(0);
			slider.setMaximum(30);
			slider.setBounds(11, 406, 295, 23);
		}
		return slider;
	}
	public JButton getBtnStartSimulation() {
		if (btnStartSimulation == null) {
			btnStartSimulation = new JButton("Start Simulation");
			btnStartSimulation.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					//Thread simulator = new Thread(startProgrammLoop());
					//startProgrammLoop();
					Thread drawer = new Thread() {
						public void run() {
							int day = 0;
							int month = Integer.parseInt(TxtMonth.getText());
							int year = Integer.parseInt(TxtYear.getText());

							createPlanets();

							for(int i = 0; i < 5; i++) {
								day += slider.getValue();
								date = new TimeCalculator(day, month, year);

								checktime();

								drawPlanets();

								try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					};
					drawer.start();
				}
			});
			btnStartSimulation.setBounds(316, 406, 173, 23);
		}
		return btnStartSimulation;
	}

	public JLabel getLblDayssec() {
		if (lblDayssec == null) {
			lblDayssec = new JLabel("Days/sec: ");
			lblDayssec.setForeground(Color.WHITE);
			lblDayssec.setBounds(10, 391, 100, 14);
		}
		return lblDayssec;
	}
	public JButton getBtnVenus() {
		if (btnVenus == null) {
			btnVenus = new JButton("Venus");
			btnVenus.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Venus.png";
					CreateJFrame();
				}
			});
			btnVenus.setForeground(Color.WHITE);
			btnVenus.setBackground(Color.ORANGE);
			btnVenus.setBounds(404, 102, 85, 21);
		}
		return btnVenus;
	}
	public JButton getBtnMars() {
		if (btnMars == null) {
			btnMars = new JButton("Mars");
			btnMars.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Mars.png";
					CreateJFrame();
				}
			});
			btnMars.setBackground(Color.RED);
			btnMars.setForeground(Color.WHITE);
			btnMars.setBounds(404, 164, 85, 21);
		}
		return btnMars;
	}
	public JButton getBtnMercury() {
		if (btnMercury == null) {
			btnMercury = new JButton("Mercury");
			btnMercury.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Mercury.png";
					CreateJFrame();
				}
			});
			btnMercury.setForeground(Color.WHITE);
			btnMercury.setBackground(Color.LIGHT_GRAY);
			btnMercury.setBounds(404, 71, 85, 21);
		}
		return btnMercury;
	}
	public JButton getBtnEarth() {
		if (btnEarth == null) {
			btnEarth = new JButton("Earth");
			btnEarth.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Earth.png";
					CreateJFrame();
				}
			});
			btnEarth.setForeground(Color.WHITE);
			btnEarth.setBackground(Color.BLUE);
			btnEarth.setBounds(404, 133, 85, 21);
		}
		return btnEarth;
	}
	public JButton getBtnSun() {
		if (btnSun == null) {
			btnSun = new JButton("Sun");
			btnSun.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Sun.png";
					CreateJFrame();
				}
			});
			btnSun.setForeground(Color.GRAY);
			btnSun.setBackground(Color.YELLOW);
			btnSun.setBounds(404, 40, 85, 21);
		}
		return btnSun;
	}

	public JButton getBtnJupiter() {
		if (btnJupiter == null) {
			btnJupiter = new JButton("Jupiter");
			btnJupiter.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Jupiter.png";
					CreateJFrame();
				}
			});
			btnJupiter.setBackground(new Color(210, 105, 30));
			btnJupiter.setForeground(new Color(255, 255, 255));
			btnJupiter.setBounds(404, 195, 85, 21);
		}
		return btnJupiter;
	}
	public JButton getBtnSaturn() {
		if (btnSaturn == null) {
			btnSaturn = new JButton("Saturn");
			btnSaturn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Saturn.png";
					CreateJFrame();
				}
			});
			btnSaturn.setBackground(new Color(139, 0, 0));
			btnSaturn.setForeground(Color.WHITE);
			btnSaturn.setBounds(404, 226, 85, 21);
		}
		return btnSaturn;
	}
	public JButton getBtnUranus() {
		if (btnUranus == null) {
			btnUranus = new JButton("Uranus");
			btnUranus.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Uranus.png";
					CreateJFrame();
				}
			});
			btnUranus.setBackground(new Color(135, 206, 250));
			btnUranus.setForeground(Color.WHITE);
			btnUranus.setBounds(404, 257, 85, 21);
		}
		return btnUranus;
	}
	public JButton getBtnNeptun() {
		if (btnNeptun == null) {
			btnNeptun = new JButton("Neptun");
			btnNeptun.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					path = "Neptune.png";
					CreateJFrame();
				}
			});
			btnNeptun.setBackground(new Color(0, 0, 139));
			btnNeptun.setForeground(Color.WHITE);
			btnNeptun.setBounds(404, 288, 85, 21);
		}
		return btnNeptun;
	}
	public JLabel getLblInfos() {
		if (lblInfos == null) {
			lblInfos = new JLabel("Infos:");
			lblInfos.setForeground(Color.WHITE);
			lblInfos.setBounds(414, 19, 46, 13);
		}
		return lblInfos;
	}

	//GUI for Planet Infos
	public JFrame CreateJFrame() {
		boolean windowexist = false;
		do {
			if(PlanetGUI == null) {
				PlanetGUI = new JFrame();
				PlanetGUI.setSize(350, 300);
				PlanetGUI.setVisible(true);
				PlanetGUI.setLocation(615, 100); 
				PlanetGUI.add(panelPlanets());
				paintComponent(this.getGraphics());
				windowexist = false;
			}
			else {
				PlanetGUI.setVisible(false);
				PlanetGUI.dispose();
				PlanetGUI = null;
				windowexist = true;
			}
		}while(windowexist != false);
		return PlanetGUI;
	}

	public JPanel panelPlanets() {
		if(panelPlanets == null) {
			panelPlanets = new JPanel();
			panelPlanets.setBorder(new LineBorder(Color.GRAY));
			panelPlanets.setBackground(Color.BLACK);
			panelPlanets.setForeground(Color.WHITE);
			panelPlanets.setBounds(0, 0, 280, 300);
			panelPlanets.add(planetInfo());
		}
		else {
			planetInfo.setText(WriteInfo());
		}
		return panelPlanets;

	}
	
	private String WriteInfo() {
		int day = Integer.parseInt(TxtDay.getText());
		int month = Integer.parseInt(TxtMonth.getText());
		int year = Integer.parseInt(TxtYear.getText());
		

		date = new TimeCalculator(day, month, year);
		
		checktime();
		createPlanets();
		
		String[] pathdivider = path.split("\\.");
		int leftnewtime, rightnewtime, newtime;
		planetInfo.setText(pathdivider[0] + ":");
		String s = "<html><u>" + pathdivider[0] + ":</u><br></br>"
				+ "------------------------------------------------------<br></br>";
		switch (pathdivider[0]) {
		case "Sun":
			s +="Apparent brightness: −26.74 mag <br></br>"
					+ "Equator Diameter: 696.342 km <br></br>"
					+ "Average Temperatur:  5.778 K<br></br>"
					+ "Mass: 1,9884E30 kg K<br></br>"
					+ "Gravitational Acceleration: 274 m/s^2<br></br>";
			break;
		case "Venus":
			s += "Position: " + degreesChangeVe + " Degrees<br></br>"
					+ "Velocity: " + VeV + " km/s<br></br>"
					+ "Mass: " + VeM + " kg<br></br>"
					+ "Big Half-Axis: " + VeBigHalfAxis + " AE<br></br>"
					+ "Eccentricity: " + VeEc + "<br></br>"
					+ "Equator Diameter: " + VeEqDi + " km<br></br>"
					+ "Gravitational Acceleration: " + VeGrAc + " m/s^2<br></br>"
					+ "Sideral Orbital Period: " + VeD + " days<br></br>"
					+ "Inclination: " + VeI + " Degrees<br></br>"
					+ "Apparent brightness: " + VeAB + " mag<br></br>"
					+ "Average Temperatur: " + VeT + " K<br></br>";
			
			degreesChangeVe += 47;

			if (degreesChangeVe >= 360) {
				int fit = (int) degreesChangeVe / 360;
				for (int i = 0; i < fit; i++) {
					degreesChangeVe -= 360;
				}
			}

			//Elongation Right Side (from Earth to Sun) 47 Degree
			 leftnewtime = venus.opposition(2, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
				s += "Next elongtion west: " + leftnewtime + " days<br></br>";


			degreesChangeVe -= 47 * 2;

			if (degreesChangeVe < 0) {
				degreesChangeVe = 360 + degreesChangeVe;
			}
			//Elongation Left Side (from Earth to Sun) 47 Degree
			rightnewtime = venus.opposition(2, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next elongtion east: " + rightnewtime + " days<br></br>";
			break;
		case "Mercury":
			s += "Position: " + degreesChangeMe + " Degrees<br></br>"
					+ "Velocity: " + MeV + " km/s<br></br>"
					+ "Mass: " + MeM + " kg<br></br>"
					+ "Big Half-Axis: " + MeBigHalfAxis + " AE<br></br>"
					+ "Eccentricity: " + MeEc + "<br></br>"
					+ "Equator Diameter: " + MeEqDi + " km<br></br>"
					+ "Gravitational Acceleration: " + MeGrAc + " m/s^2<br></br>"
					+ "Sideral Orbital Period: " + MeD + " days<br></br>"
					+ "Inclination: " + MeI + " Degrees<br></br>"
					+ "Apparent brightness: " + MeAB + " mag<br></br>"
					+ "Average Temperatur: " + MeT + " K<br></br>";
			
			//Elongation Right Side (from Earth to Sun) 28 Degree
			degreesChangeMe += 28;

			if (degreesChangeMe >= 360) {
				int fit = (int)degreesChangeMe / 360;
				for (int i = 0; i < fit; i++) {
					degreesChangeMe -= 360;
				}
			}
			leftnewtime = mercury.opposition(1, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next elongtion west: " + leftnewtime + " days<br></br>";


			degreesChangeMe -= 28 * 2;

			if (degreesChangeMe < 0) {
				degreesChangeMe = 360 + degreesChangeMe;
			}
			//Elongation Left Side (from Earth to Sun) 28 Degree
			rightnewtime = mercury.opposition(1, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next elongtion east: " + rightnewtime + " days<br></br>";
			break;
		case "Earth":
			s += "Velocity: " + EaV + " km/s<br></br>"
					+ "Mass: " + EaM + " kg<br></br>"
					+ "Big Half-Axis: " + EaBigHalfAxis + " AE<br></br>"
					+ "Eccentricity: " + EaEc + "<br></br>"
					+ "Equator Diameter: " + EaEqDi + " km<br></br>"
					+ "Gravitational Acceleration: " + EaGrAc + " m/s^2<br></br>"
					+ "Sideral Orbital Period: " + EaD + " days<br></br>"
					+ "Inclination: " + EaI + " Degrees<br></br>"
					+ "Average Temperatur: " + EaT + " K<br></br>";
			break;
		case "Mars":
			s += "Position: " + degreesChangeMa + " Degrees<br></br>"
				+ "Velocity: " + MaV + " km/s<br></br>"
				+ "Mass: " + MaM + " kg<br></br>"
				+ "Big Half-Axis: " + MaBigHalfAxis + " AE<br></br>"
				+ "Eccentricity: " + MaEc + "<br></br>"
				+ "Equator Diameter: " + MaEqDi + " km<br></br>"
				+ "Gravitational Acceleration: " + MaGrAc + " m/s^2<br></br>"
				+ "Sideral Orbital Period: " + MaD + " days<br></br>"
				+ "Inclination: " + MaI + " Degrees<br></br>"
				+ "Apparent brightness: " + MaAB + " mag<br></br>"
				+ "Average Temperatur: " + MaT + " K<br></br>";
			
			newtime = mars.opposition(4, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next opposition: " + newtime + " days<br></br>";
			break;
		case "Jupiter":
			s += "Position: " + degreesChangeJu + " Degrees<br></br>"
					+ "Velocity: " + JuV + " km/s<br></br>"
					+ "Mass: " + JuM + " kg<br></br>"
					+ "Big Half-Axis: " + JuBigHalfAxis + " AE<br></br>"
					+ "Eccentricity: " + JuEc + "<br></br>"
					+ "Equator Diameter: " + JuEqDi + " km<br></br>"
					+ "Gravitational Acceleration: " + JuGrAc + " m/s^2<br></br>"
					+ "Sideral Orbital Period: " + JuD + " days<br></br>"
					+ "Inclination: " + JuI + " Degrees<br></br>"
					+ "Apparent brightness: " + JuAB + " mag<br></br>"
					+ "Average Temperatur: " + JuT + " K<br></br>";
			
			newtime = jupiter.opposition(5, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next opposition: " + newtime + " days<br></br>";
			break;
		case "Saturn":
			s += "Position: " + degreesChangeSa + " Degrees<br></br>"
					+ "Velocity: " + SaV + " km/s<br></br>"
					+ "Mass: " + SaM + " kg<br></br>"
					+ "Big Half-Axis: " + SaBigHalfAxis + " AE<br></br>"
					+ "Eccentricity: " + SaEc + "<br></br>"
					+ "Equator Diameter: " + SaEqDi + " km<br></br>"
					+ "Gravitational Acceleration: " + SaGrAc + " m/s^2<br></br>"
					+ "Sideral Orbital Period: " + SaD + " days<br></br>"
					+ "Inclination: " + SaI + " Degrees<br></br>"
					+ "Apparent brightness: " + SaAB + " mag<br></br>"
					+ "Average Temperatur: " + SaT + " K<br></br>";
			
			newtime = saturn.opposition(6, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next opposition: " + newtime + " days<br></br>";
			break;
		case "Uranus":
			s += "Position: " + degreesChangeUr + " Degrees<br></br>"
					+ "Velocity: " + UrV + " km/s<br></br>"
					+ "Mass: " + UrM + " kg<br></br>"
					+ "Big Half-Axis: " + UrBigHalfAxis + " AE<br></br>"
					+ "Eccentricity: " + UrEc + "<br></br>"
					+ "Equator Diameter: " + UrEqDi + " km<br></br>"
					+ "Gravitational Acceleration: " + UrGrAc + " m/s^2<br></br>"
					+ "Sideral Orbital Period: " + UrD + " days<br></br>"
					+ "Inclination: " + UrI + " Degrees<br></br>"
					+ "Apparent brightness: " + UrAB + " mag<br></br>"
					+ "Average Temperatur: " + UrT + " K<br></br>";
			
			newtime = uranus.opposition(7, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next opposition: " + newtime + " days<br></br>";
			break;
		case "Neptune":
			s += "Position: " + degreesChangeNe + " Degrees<br></br>"
					+ "Velocity: " + NeV + " km/s<br></br>"
					+ "Mass: " + NeM + " kg<br></br>"
					+ "Big Half-Axis: " + NeBigHalfAxis + " AE<br></br>"
					+ "Eccentricity: " + NeEc + "<br></br>"
					+ "Equator Diameter: " + NeEqDi + " km<br></br>"
					+ "Gravitational Acceleration: " + NeGrAc + " m/s^2<br></br>"
					+ "Sideral Orbital Period: " + NeD + " days<br></br>"
					+ "Inclination: " + NeI + " Degrees<br></br>"
					+ "Apparent brightness: " + NeAB + " mag<br></br>"
					+ "Average Temperatur: " + NeT + " K<br></br>";
			
			newtime = neptun.opposition(8, degreesChangeMe, degreesChangeVe, degreesChangeEa,
					degreesChangeMa, degreesChangeJu, degreesChangeSa, degreesChangeUr, degreesChangeNe,
					MeDegrees, VeDegrees, EaDegrees, MaDegrees, JuDegrees, SaDegrees, UrDegrees, NeDegrees,
					date);
			s += "Next opposition: " + newtime + " days<br></br>";
			break;
		}
		
		s += "</html>";
		
		return s;
	}
	
	public JLabel planetInfo() {
		if(planetInfo == null) {
			planetInfo = new JLabel();
			planetInfo.setBorder(new LineBorder(Color.GRAY));
			planetInfo.setBackground(Color.BLACK);
			planetInfo.setForeground(Color.WHITE);
			planetInfo.setBounds(20, 0, 350, 300);
			//planetInfo.setHorizontalAlignment(SwingConstants.LEFT);
		}
		planetInfo.setText(WriteInfo());
		return planetInfo;
	}
	public void paintComponent(Graphics g) {
		try {
			image = ImageIO.read(new File("src/Solar_Navigator/pictures/" + path));

			super.paintComponents(g);

			g.drawImage(image, 415, 365, 70, 70, null);
			g.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public JLabel getLblSelectedPlanet() {
		if (lblSelectedPlanet == null) {
			lblSelectedPlanet = new JLabel("Selected Planet:");
			lblSelectedPlanet.setForeground(Color.WHITE);
			lblSelectedPlanet.setBounds(404, 320, 96, 14);
		}
		return lblSelectedPlanet;
	}
}
