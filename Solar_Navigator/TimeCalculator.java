package Solar_Navigator;

public class TimeCalculator {

	private int day_;
	private int month_;
	private int year_;

	public TimeCalculator(int day, int month, int year) {
		this.day_ = day;
		this.month_ = month;
		this.year_ = year;
	}


	public double gettime() {
		int DayM = 0;
		if (month_ == 2) {
			DayM += 31;
		}
		else if (month_ == 3) {
			DayM += 59.256;
		}
		else if (month_ == 4) {
			DayM += 90.256;
		}
		else if (month_ == 5) {
			DayM += 120.256;
		}
		else if (month_ == 6) {
			DayM += 151.256;
		}
		else if (month_ == 7) {
			DayM += 181.256;
		}
		else if (month_ == 8) {
			DayM += 212.256;
		}
		else if (month_ == 9) {
			DayM += 243.256;
		}
		else if (month_ == 10) {
			DayM += 273.256;
		}
		else if (month_ == 11) {
			DayM += 304.256;
		}
		else if (month_ == 12) {
			DayM += 334.256;
		}

		int yeardiff = year_ - 2018;
		double DayY = (yeardiff * 365.256);
		double time = (day_ - 1) + DayM + DayY;

		return time;
	}

}
