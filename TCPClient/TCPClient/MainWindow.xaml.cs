﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TCPClient
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private NewClient tcp;
        public delegate void EndThreadEventDelegate();
        Thread checkRTX;


        public MainWindow()
        {
            InitializeComponent();

            tcp = new NewClient();

            tcp.ConnectEvent += Newthread_ConnectEvent;

            ThreadOutput ChangeOutput = new ThreadOutput();

            ChangeOutput.EventActive += CheckOutput;

            checkRTX = new Thread(ChangeOutput.ThreadActive);

            checkRTX.Start();

        }

        private void CheckOutput()
        {
            if (OutputTxt.Dispatcher.CheckAccess() == false)
            {
                this.Dispatcher.BeginInvoke(new Action(CheckOutput));
                return;
            }

            if (tcp.outputTxt != null)
            {
                OutputTxt.Text = tcp.outputTxt;
            }
        }

        private void Connect_Button_Click(object sender, RoutedEventArgs e)
        {
            if (Connection.Content.Equals("Connect"))
            {
                try
                {
                    tcp.HostName = IP.Text;
                    tcp.HostPort = Int32.Parse(Port.Text);
                }
                catch (Exception)
                {
                    Console.WriteLine("Wrong Input");
                    return;
                }

                tcp.Open();

            }
            else
            {
                tcp.Close();
                Connection.Content = "Connect";

                isConnected.Foreground = new SolidColorBrush(Colors.Red);
                isConnected.Content = "Disconnected";
            }
        }

        private void Newthread_ConnectEvent()
        {
            if (isConnected.Dispatcher.CheckAccess() == false)
            {
                this.Dispatcher.BeginInvoke(new Action(Newthread_ConnectEvent));
                return;
            }


            isConnected.Foreground = new SolidColorBrush(Colors.Green);
            isConnected.Content = "Connected";

            Connection.Content = "Disconnect";
            tcp.PollingTime = 100;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            tcp.Write(InputTxt.Text);
        }
    }
    public class ThreadOutput
    {
        public delegate void ActiveEventDelegate();
        public event ActiveEventDelegate EventActive;
        public void ThreadActive()
        {
            while (true)
            {
                if (EventActive != null)
                {
                    EventActive.Invoke();
                }

                Thread.Sleep(100);
            }
        }

    }
}