﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

/// <summary>
/// Testprogramm für LoadPort
/// </summary>
namespace TCPClient
{
    public class NewClient
    {
        //öffentliche Variablen
        public bool Connected { get; private set; }
        public string HostName { get; set; }
        public int HostPort { get; set; }
        public int PollingTime { get; set; } = 0;
        public string outputTxt { get; set; }

        //Event
        public delegate void ConnectedEventDelegate();
        public event ConnectedEventDelegate ConnectEvent;

        //private Variablen
        private Thread writer;
        private bool endthread;
        private NetworkStream networkStream;
        private TcpClient _client;
        private Dictionary<string, string> errorMessages = new Dictionary<string, string>();
        
        private void InvokeThread()
        {
                writer = new Thread(KeepAlive)
                {
                    Name = "IP-Connector",
                    IsBackground = true
                };
                ReadRTX();

                writer.Start();
        }

        private void KeepAlive()
        {
            try
            {
                endthread = false;

                while (endthread != true)
                {
                    ReadRTX();
                    Thread.Sleep(100);
                }

                Console.WriteLine("Thread ended");

            }
            catch (Exception)
            {
                Console.WriteLine("ERROR with Keep-Alive");
            }

        }

        /// <summary>
        /// liest einkommente Signale
        /// </summary>
        private void ReadRTX()
        {
            try
            {
                StreamReader reader = new StreamReader(networkStream);
                StringBuilder response = new StringBuilder();

                if (networkStream.DataAvailable == true)
                {
                    while (reader.Peek() > 0)
                    {
                        response.Append(reader.ReadLine() + "\r\n");
                    }
                    outputTxt += response.ToString();
                    
                }

            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }

        private void EndThread()
        {
            endthread = true;
        }

        public bool Write(string input)
        {
            try
            {
                if (_client != null && _client.Connected)
                {
                    StreamWriter writer = new StreamWriter(networkStream);

                    writer.WriteLine(input);
                    writer.Flush();
                    return true;
                }
                Console.WriteLine("Not connected");
                return false;
            }
            catch (Exception)
            {
                Console.WriteLine("Not able to send command");
                return false;
            }
        }

        public bool Open()
        {
            try
            {
                _client = new TcpClient();

                _client.Connect(new IPEndPoint(IPAddress.Parse(HostName), HostPort));
                networkStream = _client.GetStream();

                if (ConnectEvent != null)
                {
                    ConnectEvent.Invoke();
                }
                InvokeThread();
                Connected = true;
                return true;
            }
            catch (Exception)
            {
                Console.WriteLine("couldn't connect to TCP client");
                return false;
            }
        }
        public bool Close()
        {
            EndThread();

            while (writer.IsAlive == true)
                Thread.Sleep(10);

            _client.Close();
            Connected = false;
            return true;
        }
    }
}

